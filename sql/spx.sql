DELETE from spx where bid = 0 AND ask = 0;
DELETE FROM spx where expiration > at + interval '3 month';

DELETE From spx where strike >= underlying_bid + 200;
DELETE From spx where strike <= underlying_bid - 200;

DELETE FROM spx where option_type = 'C' and delta > 0.8;
DELETE FROM spx where option_type = 'P' and delta < -0.8;

DELETE FROM spx where option_type = 'C' and strike < underlying_bid - 100;
DELETE FROM spx where option_type = 'P' and strike > underlying_bid + 100;

select DATE(at)
, count(1)
from spx
group by 1
order by 1

#puts
SELECT DATE(s.at)
, s.bid+(s.ask-s.bid)/2- s2.bid - (s2.ask - s2.bid)/2 diff
, s.bid+(s.ask-s.bid)/2 start_price
, s2.bid+ (s2.ask - s2.bid)/2 close_price
, s.ask - s.bid slippage
, s2.ask- s2.bid slippage2
, s.expiration
, s.expiration - DATE(s.at) days
, s.strike
, s.option_type
, s.volume
, s.underlying_price starting
, s2.underlying_price closing_price
, -s.theta/s.vega tv
from spx s
join spx s2
  ON s2.option_type = s.option_type
  AND s2.expiration = s.expiration
  AND s2.strike = s.strike
  AND s2.at = '2017-03-14 16:05:00'
where s.at = '2017-03-14 09:40:00'
and s.option_type = 'P'
and s.delta > -0.5
and s2.ask- s2.bid<= 0.4
and s.ask- s.bid<= 0.4
and s.bid+(s.ask-s.bid)/2 < 10
and s.expiration < s.at + interval '1 month'
and s.volume >= 10
AND s.underlying_price - s.strike <= 20
order by s.bid+(s.ask-s.bid)/2- s2.bid - (s2.ask - s2.bid)/2 desc
limit 1

#calls
SELECT DATE(s.at)
, s.bid+(s.ask-s.bid)/2- s2.bid - (s2.ask - s2.bid)/2 diff
, s.bid+(s.ask-s.bid)/2 start_price
, s2.bid+ (s2.ask - s2.bid)/2 close_price
, s.ask - s.bid slippage
, s2.ask- s2.bid slippage2
, s.expiration
, s.expiration - DATE(s.at) days
, s.strike
, s.option_type
, s.volume
, s.underlying_price starting
, s2.underlying_price closing_price
, -s.theta/s.vega tv
from spx s
join spx s2
  ON s2.option_type = s.option_type
  AND s2.expiration = s.expiration
  AND s2.strike = s.strike
  AND s2.at = '2017-03-14 16:05:00'
where s.at = '2017-03-14 09:40:00'
and s.option_type = 'C'
and s.delta < 0.5
and s2.ask- s2.bid<= 0.4
and s.ask- s.bid<= 0.4
and s.bid+(s.ask-s.bid)/2 < 10
and s.expiration < s.at + interval '1 month'
and s.volume >= 10
AND s.strike - s.underlying_price <= 20
order by s.bid+(s.ask-s.bid)/2- s2.bid - (s2.ask - s2.bid)/2 desc
limit 1

#lows
select 2.45- s.bid+(s.ask-s.bid)/2 low
 , s.at
 , s.bid+(s.ask-s.bid)/2 mid
, s.volume volume_low
, s.underlying_price price_low
from spx s
where DATE(s.at) = '2017-01-18'
AND s.expiration = '2017-01-18'
and s.option_type = 'P'
and s.strike = 2265
AND s.at > '2017-01-18 09:40:00'
AND s.at < '2017-01-18 16:05:00'
ORDER BY 2.45 - s.bid+(s.ask-s.bid)/2
limit 1

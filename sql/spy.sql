select at, strike, expiration, option_type, bid, ask, delta, gamma, vega, theta, theta/vega tv, underlying_price
from spy_historic_options
where at BETWEEN '2016-08-25' AND '2016-08-26'
AND (
(strike = 220 and option_type = 'C') 
  OR 
(strike = 214 and option_type = 'P')
)
AND expiration= '2016-09-23'
AND expiration BETWEEN at + interval '15 days' AND at + interval '50 days'

select at, strike, expiration, option_type, bid, ask, delta, underlying_price
from spy_historic_options
where DATE(at) = '2017-01-06'
AND ((strike = 225 AND option_type = 'P') --226.14-1
OR (strike = 227 AND option_type = 'C'))
AND expiration = DATE(at)
order by at

select DATE(s.at)
, s.bid+(s.ask-s.bid)/2- s2.bid - (s2.ask - s2.bid)/2 price
, s.underlying_price
, s2.underlying_price
, s.strike
from spy_historic_options s
join spy_historic_options s2
  ON DATE(s2.at) = DATE(s.at)
  AND s2.expiration = s.expiration
  AND s2.strike = s.strike
  AND s2.option_type = s.option_type
  AND extract(hour from s2.at) = 16
  AND extract(minute from s2.at) = 15
where extract(hour from s.at) = 9
AND extract(minute from s.at) = 45
AND s.expiration = DATE(s.at)
AND s.option_type = 'C'
and s.strike BETWEEN s.underlying_price + 0.5 AND s.underlying_price + 1

select DATE(s.at)
, s.bid+(s.ask-s.bid)/2- s2.bid - (s2.ask - s2.bid)/2 price
, s.underlying_price
, s2.underlying_price
, s.strike
from spy_historic_options s
join spy_historic_options s2
  ON DATE(s2.at) = DATE(s.at)
  AND s2.expiration = s.expiration
  AND s2.strike = s.strike
  AND s2.option_type = s.option_type
  AND extract(hour from s2.at) = 16
  AND extract(minute from s2.at) = 15
where extract(hour from s.at) = 9
AND extract(minute from s.at) = 45
AND s.expiration = DATE(s.at)
AND s.option_type = 'P'
AND s.strike BETWEEN s.underlying_price - 1 AND s.underlying_price - 0.5

SELECT calls.*
, puts.*
FROM (
SELECT DATE(s.at)
, s.bid+(s.ask-s.bid)/2- s2.bid - (s2.ask - s2.bid)/2 diff_put
, s.bid+(s.ask-s.bid)/2 start_price_put
, s2.bid+ (s2.ask - s2.bid)/2 close_price_put
, s.ask - s.bid slippage_put
, s2.ask- s2.bid slippage2_put
, s.expiration
, s.expiration - DATE(s.at) days
, s.strike
, s.option_type
, s.volume
, s2.volume
, s.underlying_price starting_price
, s2.underlying_price closing_price
from spx s
join spx s2
  ON s2.option_type = s.option_type
  AND s2.expiration = s.expiration
  AND s2.strike = s.strike
  AND s2.at = '2017-03-14 16:05:00'
where s.at = '2017-03-14 09:40:00'
and s.option_type = 'P'
and s.expiration <= s.at + interval '5 days'
AND s.underlying_price - s.strike <= 15
AND s.underlying_price - s.strike > 10
order by s.expiration
limit 1
) calls
JOIN 
(
SELECT s.bid+(s.ask-s.bid)/2- s2.bid - (s2.ask - s2.bid)/2 diff_call
, s.bid+(s.ask-s.bid)/2 start_price
, s2.bid+ (s2.ask - s2.bid)/2 close_price
, s.ask - s.bid slippage
, s2.ask- s2.bid slippage2
, s.expiration
, s.strike
, s.option_type
, s.volume
, s2.volume
from spx s
join spx s2
  ON s2.option_type = s.option_type
  AND s2.expiration = s.expiration
  AND s2.strike = s.strike
  AND s2.at = '2017-03-14 16:05:00'
where s.at = '2017-03-14 09:40:00'
and s.option_type = 'C'
and s.expiration <= s.at + interval '5 days'
AND s.strike - s.underlying_price  <= 15
AND s.strike - s.underlying_price > 10
order by s.expiration
limit 1
) puts
ON puts.expiration = calls.expiration


#6.90, not 5.075 as expected
SELECT MAX((s.bid+s.ask + s2.bid +s2.bid+s2.ask)/2) mid
from spx s
join spx s2
  ON s2.at = s.at
  AND s2.expiration = s.expiration
  AND s2.option_type = 'C'
  AND s2.strike = 2160
where DATE(s.at) = '2016-10-05'
AND s.at > '2016-10-05 09:40:00'
AND s.at < '2016-10-05 16:05:00'
and s.option_type = 'P'
and s.expiration = '2016-10-05'
AND s.strike = 2155


SELECT s.at
, s.bid, s.ask
, s2.bid, s2.ask
from spx s
join spx s2
  ON s2.at = s.at
  AND s2.expiration = s.expiration
  AND s2.option_type = 'C'
  AND s2.strike = 2160
where DATE(s.at) = '2016-10-05'
AND s.at > '2016-10-05 09:40:00'
AND s.at < '2016-10-05 16:05:00'
and s.option_type = 'P'
and s.expiration = '2016-10-05'
AND s.strike = 2155
order by at

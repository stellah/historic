require 'csv'


# underlying_symbol,quote_datetime
# root,expiration,strike,option_type,
# open,high,low,close,
# trade_volume,bid_size,bid,ask_size,ask,underlying_bid,underlying_ask,implied_underlying_price,
# active_underlying_price,implied_volatility,delta,gamma,theta,vega,rho
# cd ~/Downloads
# cat UnderlyingOptionsIntervalsCalcs.csv | psql historic_dev -c "COPY spx (symbol, at, root, expiration, strike, option_type,open,high,low,close,volume,bid_size,bid,ask_size,ask,underlying_bid,underlying_ask,implied_underlying_price,underlying_price,iv,delta,gamma,theta,vega,rho) FROM STDIN WITH CSV"

# DELETE from spx where bid = 0 AND ask = 0;
# 84188 rows
FILE_DIR = "/Users/snugs/Downloads/spx"

class Spx
  def self.strike(striker)
    CSV.open("/Users/snugs/Downloads/spx_strikes.csv", "wb") do |csv|
      start_date = Date.new(2016,10,4)
      end_date = Date.new(2016,11,4)
      striker2 = striker-5
      while start_date < end_date
        start_date += 1
        next if start_date.saturday? || start_date.sunday? || start_date == Date.new(2016,10,21)
        month = pad(start_date.month)
        day = pad(start_date.day)
        year = start_date.year
        
        sql = "
        SELECT calls.*
        , puts.*
        FROM (
        SELECT DATE(s.at)
        , s.bid+(s.ask-s.bid)/2- s2.bid - (s2.ask - s2.bid)/2 diff_put
        , s.bid+(s.ask-s.bid)/2 start_price_put
        , s2.bid+ (s2.ask - s2.bid)/2 close_price_put
        , s.ask - s.bid slippage_put
        , s2.ask- s2.bid slippage2_put
        , s.expiration
        , s.expiration - DATE(s.at) days
        , s.strike
        , s.option_type
        , s.volume
        , s2.volume
        , s.underlying_bid starting_price
        , s2.underlying_bid closing_price
        from spx s
        join spx s2
          ON s2.option_type = s.option_type
          AND s2.expiration = s.expiration
          AND s2.strike = s.strike
          AND s2.at = '#{year}-#{month}-#{day} 16:05:00'
        where s.at = '#{year}-#{month}-#{day} 09:40:00'
        and s.option_type = 'P'
        and s.expiration <= s.at + interval '5 days'
        AND s.underlying_bid - s.strike <= #{striker}
        AND s.underlying_bid - s.strike > #{striker2}
        order by s.expiration DESC
        limit 1
        ) calls
        JOIN 
        (
        SELECT s.bid+(s.ask-s.bid)/2- s2.bid - (s2.ask - s2.bid)/2 diff_call
        , s.bid+(s.ask-s.bid)/2 start_price
        , s2.bid+ (s2.ask - s2.bid)/2 close_price
        , s.ask - s.bid slippage
        , s2.ask- s2.bid slippage2
        , s.expiration
        , s.strike
        , s.option_type
        , s.volume
        , s2.volume
        from spx s
        join spx s2
          ON s2.option_type = s.option_type
          AND s2.expiration = s.expiration
          AND s2.strike = s.strike
          AND s2.at = '#{year}-#{month}-#{day} 16:05:00'
        where s.at = '#{year}-#{month}-#{day} 09:40:00'
        and s.option_type = 'C'
        and s.expiration <= s.at + interval '5 days'
        AND s.strike - s.underlying_bid  <= #{striker}
        AND s.strike - s.underlying_bid > #{striker2}
        order by s.expiration DESC
        limit 1
        ) puts
        ON puts.expiration = calls.expiration
        "
        records_array = ActiveRecord::Base.connection.execute(sql)
        records_array.values.each do |row|
          p row
          csv << row
        end
      end
    end
  end

  def self.each_day
    CSV.open("/Users/snugs/Downloads/spx.csv", "wb") do |csv|
      start_date = Date.new(2017,1,17)
      end_date = Date.new(2017,3,17)
      while start_date < end_date
        start_date += 1
        next if start_date.saturday? || start_date.sunday? || start_date == Date.new(2017,2,20)
        month = pad(start_date.month)
        day = pad(start_date.day)
        year = start_date.year

        option_type = "P"
        
        sql = "SELECT DATE(s.at)
        , s.bid+(s.ask-s.bid)/2- s2.bid - (s2.ask - s2.bid)/2 diff
        , s.bid+(s.ask-s.bid)/2 start_price
        , s2.bid+ (s2.ask - s2.bid)/2 close_price
        , s.ask - s.bid slippage
        , s2.ask- s2.bid slippage2
        , s.expiration
        , s.expiration - DATE(s.at) days
        , s.strike
        , s.option_type
        , s.volume
        , s.underlying_bid starting
        , s2.underlying_bid closing_price
        , -s.theta/s.vega tv
        from spx s
        join spx s2
          ON s2.option_type = s.option_type
          AND s2.expiration = s.expiration
          AND s2.strike = s.strike
          AND s2.at = '#{year}-#{month}-#{day} 16:05:00'
        where s.at = '#{year}-#{month}-#{day} 09:40:00'
        and s.option_type = '#{option_type}'
        and s.delta > -0.5
        and s2.ask- s2.bid<= 0.4
        and s.ask- s.bid<= 0.4
        and s.bid+(s.ask-s.bid)/2 < 10
        and s.expiration < s.at + interval '1 month'
        and s.volume >= 10
        AND s.underlying_bid - s.strike <= 20
        order by s.bid+(s.ask-s.bid)/2- s2.bid - (s2.ask - s2.bid)/2 desc
        limit 1
        "
        records_array = ActiveRecord::Base.connection.execute(sql)
        results = records_array.values.first

        at = results[0]
        price = results[2]
        expiry = results[6]
        strike = results[8]
        sql2 = "select #{price} - s.bid+(s.ask-s.bid)/2
        , s.at
        , s.bid+(s.ask-s.bid)/2
        , s.volume volume_low
        , s.underlying_bid price_low
        from spx s
        where DATE(s.at) = '#{at}'
        AND s.expiration = '#{expiry}'
        and s.option_type = '#{option_type}'
        and s.strike = #{strike}
        AND s.at > '#{at} 09:40:00'
        AND s.at < '#{at} 16:05:00'
        ORDER BY #{price} - s.bid+(s.ask-s.bid)/2
        limit 1
        "
        records_array = ActiveRecord::Base.connection.execute(sql2)
        results += records_array.values.first
        csv << results

        option_type = "C"

        sql = "SELECT DATE(s.at)
        , s.bid+(s.ask-s.bid)/2- s2.bid - (s2.ask - s2.bid)/2 diff
        , s.bid+(s.ask-s.bid)/2 start_price
        , s2.bid+ (s2.ask - s2.bid)/2 close_price
        , s.ask - s.bid slippage
        , s2.ask- s2.bid slippage2
        , s.expiration
        , s.expiration - DATE(s.at) days
        , s.strike
        , s.option_type
        , s.volume
        , s.underlying_bid starting
        , s2.underlying_bid closing_price
        , -s.theta/s.vega tv
        from spx s
        join spx s2
          ON s2.option_type = s.option_type
          AND s2.expiration = s.expiration
          AND s2.strike = s.strike
          AND s2.at = '#{year}-#{month}-#{day} 16:05:00'
        where s.at = '#{year}-#{month}-#{day} 09:40:00'
        and s.option_type = '#{option_type}'
        and s.delta < 0.5
        and s2.ask- s2.bid<= 0.4
        and s.ask- s.bid<= 0.4
        and s.bid+(s.ask-s.bid)/2 < 10
        and s.expiration < s.at + interval '1 month'
        and s.volume >= 10
        AND s.strike - s.underlying_bid <= 20
        order by s.bid+(s.ask-s.bid)/2- s2.bid - (s2.ask - s2.bid)/2 desc
        limit 1
        "
        records_array = ActiveRecord::Base.connection.execute(sql)
        results = records_array.values.first

        at = results[0]
        price = results[2]
        expiry = results[6]
        strike = results[8]
        sql2 = "select #{price} - s.bid+(s.ask-s.bid)/2
        , s.at
        , s.bid+(s.ask-s.bid)/2
        , s.volume
        , s.underlying_bid
        from spx s
        where DATE(s.at) = '#{at}'
        AND s.expiration = '#{expiry}'
        and s.option_type = '#{option_type}'
        and s.strike = #{strike}
        AND s.at > '#{at} 09:40:00'
        AND s.at < '#{at} 16:05:00'
        ORDER BY #{price} - s.bid+(s.ask-s.bid)/2
        limit 1
        "
        records_array = ActiveRecord::Base.connection.execute(sql2)
        results += records_array.values.first
        csv << results

        csv << []
        p results
      end #while
    end
  end

  def self.pad int
    return "0#{int}" if int < 10
    "#{int}"
  end

  def self.upload
    conn = ActiveRecord::Base.connection.raw_connection
    Dir["#{FILE_DIR}/*.csv"].each do |file|
      filename = File.basename(file)
      p filename
      #mapping for these last 6 columns incorrect, but not used
      #number_of_exchanges,{exchange,bid_size,bid,ask_size,ask}[number_of_exchanges]
      #10/21/2016 weird
      conn.exec("COPY spx (symbol,at,root,expiration,strike,option_type,open,high,low,close,volume,bid_size,bid,ask_size,ask,underlying_bid,underlying_ask,iv,gamma,theta,vega,rho,implied_underlying_price) FROM '#{FILE_DIR}/#{filename}' CSV HEADER")
    end
  end

end
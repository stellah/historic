require 'csv'


# underlying_symbol,quote_datetime
# root,expiration,strike,option_type,
# open,high,low,close,
# trade_volume,bid_size,bid,ask_size,ask,underlying_bid,underlying_ask,implied_underlying_price,
# active_underlying_price,implied_volatility,delta,gamma,theta,vega,rho
# cd /Volumes/WD\ Elements/
# cat UnderlyingOptionsIntervalsCalcs.csv | psql historic_dev -c "COPY spy_historic_options (symbol, at, root, expiration, strike, option_type,open,high,low,close,volume,bid_size,bid,ask_size,ask,underlying_bid,underlying_ask,implied_underlying_price,underlying_price,iv,delta,gamma,theta,vega,rho) FROM STDIN WITH CSV"

# 1000000
# "ERROR:  invalid input syntax for type timestamp: \"quote_datetime\"\nCONTEXT:  COPY spy_historic_options, line 1, column at: \"quote_datetime\"\n"
# 2000000
# First 1,000,000 rows missed
# ""
#  id |         at          | expiration | strike  | option_type | volume |  bid   |  ask   | bid_size | ask_size | underlying_price | underlying_bid | underlying_ask |   iv   | delta  | gamma  |  theta  |  vega   |   rho   | symbol | root |  open  |  high  |  low   | close  | implied_underlying_price | created_at | updated_at
# ----+---------------------+------------+---------+-------------+--------+--------+--------+----------+----------+------------------+----------------+----------------+--------+--------+--------+---------+---------+---------+--------+------+--------+--------+--------+--------+--------------------------+------------+------------
#   2 | 2005-03-10 16:15:00 | 2006-12-16 | 140.000 | C           |      0 | 2.7000 | 2.8000 |     1437 |      500 |         121.2900 |       121.2500 |       121.2600 | 0.1201 | 0.2535 | 1.6272 | -0.5840 | 50.9026 | 49.5536 | SPY    | CYY  | 0.0000 | 0.0000 | 0.0000 | 0.0000 |                 117.1918 |

# 238,000,000 records removed
# DELETE 23,297,867 where bid = 0 AND ask <= 0.05;
# 2005-03-10 09:45:00 MIN(at)

class OptionsHistoric
  def self.upload
    conn = ActiveRecord::Base.connection.raw_connection
    conn.exec("COPY spy_historic_options (symbol,at,root,expiration,strike,option_type,open,high,low,close,volume,bid_size,bid,ask_size,ask,underlying_bid,underlying_ask,implied_underlying_price,underlying_price,iv,delta,gamma,theta,vega,rho) FROM STDIN WITH CSV")

    i = 0
    file = File.open("/Volumes/WD\ Elements/UnderlyingOptionsIntervalsCalcs.csv", 'r')
    while !file.eof?
      # Add row to copy data
      conn.put_copy_data(file.readline) if i > 0
      i+=1
      if i % 1000000 == 0
        puts "#{i}"
        conn.put_copy_end
        # Display any error messages
        while res = conn.get_result
          p res.error_message
        end

        conn = ActiveRecord::Base.connection.raw_connection
        conn.exec("COPY spy_historic_options (symbol,at,root,expiration,strike,option_type,open,high,low,close,volume,bid_size,bid,ask_size,ask,underlying_bid,underlying_ask,implied_underlying_price,underlying_price,iv,delta,gamma,theta,vega,rho) FROM STDIN WITH CSV")
      end
    end

  end
end
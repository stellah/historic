require 'net/ftp'

class FtpDownload
  def self.spx
    ftp = Net::FTP.new
    ftp.connect("ftp.datashop.livevol.com", 21)
    ftp.login("stellah@gmail.com","")
    # ftp.chdir("/order_000001414/item_000002418/")
    ftp.chdir("order_000001362/item_000002318/")
    ftp.passive = true
    fileList = ftp.list('Underlying*.zip')
    fileList.each do |file|
      file_name = file[/.*(\d{2}):(\d{2})\s{1}(?<file>.+)$/,1]
      ftp.getbinaryfile(file_name, "/Users/snugs/Downloads/spx/#{file_name}")
    end
    ftp.close
  end

  def self.gmcr
    ftp = Net::FTP.new
    ftp.connect("ftp.datashop.livevol.com", 21)
    ftp.login("stellah@gmail.com","")
    ftp.chdir("/order_000001401/item_000002404/")
    ftp.passive = true
    fileList = ftp.list('Underlying*.zip')
    fileList.each do |file|
      file_name = file[/.*(\d{2}):(\d{2})\s{1}(?<file>.+)$/,1]
      ftp.getbinaryfile(file_name, "/Users/snugs/Downloads/gmcr/#{file_name}")
    end
    ftp.close
  end
end

# for i in *.zip
# do
#     unzip $i
# done
# rm *.zip

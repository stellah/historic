require 'csv'

class SpxLows
  def self.lows_1d
    CSV.foreach("/Users/snugs/trading/historic/1d_strangle.csv", headers: true) do |row|
      date = row["date"]
      expiry = row["expiration"]
      put_strike = row["strike"]
      call_strike = row["strike_call"]

      sql = "
      SELECT MAX((s.bid+s.ask + s2.bid +s2.bid+s2.ask)/2)
      from spx s
      join spx s2
        ON s2.at = s.at
        AND s2.expiration = s.expiration
        AND s2.option_type = 'C'
        AND s2.strike = #{call_strike}
      where DATE(s.at) = '#{date}'
      AND s.at > '#{date} 09:40:00'
      AND s.at < '#{date} 16:05:00'
      and s.option_type = 'P'
      and s.expiration = '#{expiry}'
      AND s.strike = #{put_strike}
      "
      records_array = ActiveRecord::Base.connection.execute(sql)
      low = records_array.values.first
      p "#{date}, #{low.first}"
    end
  end
end
# 
# 10/05/16, 9.925
# 10/06/16, 27.85
# 10/07/16, 27.625
# 10/10/16, 7.325
# 10/11/16, 44.175

require 'csv'


# underlying_symbol,quote_datetime, sequence_number
# root,expiration,strike,option_type,
# exchange_id,trade_size,trade_price,trade_condition_id,canceled_trade_condition_id,best_bid,best_ask,
# underlying_bid,underlying_ask,number_of_exchanges,exchange,bid_size,bid,ask_size,ask
# cd ~/Downloads
# cat UnderlyingOptionsTrades.csv | psql historic_dev -c "COPY trade_times (symbol, at, sequence,root,expiration,strike,option_type,exchange_id,trade_size,trade_price,trade_condition_id,canceled_trade_condition_id,best_bid,best_ask,underlying_bid,underlying_ask,number_of_exchanges,exchange,bid_size,bid,ask_size,ask) FROM STDIN WITH CSV"

GMCR_DIR = "/Users/snugs/Downloads/gmcr"

class TradeTime
  def self.gmcr_strip
    Dir["#{GMCR_DIR}/*.csv"].each do |file|
      filename = File.basename(file)
      # next unless filename.starts_with?("Underlying")

      columns = ["underlying_symbol","quote_datetime",	"sequence_number",	"root",	"expiration",	"strike",
      "option_type",	"exchange_id",	"trade_size",	"trade_price",	"trade_condition_id",
      "canceled_trade_condition_id",	"best_bid",	"best_ask",	"underlying_bid",	"underlying_ask",
      "number_of_exchanges",	"{exchange",	"bid_size",	"bid",	"ask_size", "ask}[number_of_exchanges]"]

      CSV.open("#{gmcr_dir}2/#{filename}", "wb") do |csv|
        CSV.foreach(file, headers: true) do |row|
          csv << columns.map { |col| row[col] }
        end
      end
    end
  end

  def self.upload
    conn = ActiveRecord::Base.connection.raw_connection
    Dir["#{GMCR_DIR}/*.csv"].each do |file|
      filename = File.basename(file)
      conn.exec("COPY trade_times (symbol,at,sequence,root,expiration,strike,option_type,exchange_id,trade_size,trade_price,trade_condition_id,canceled_trade_condition_id,best_bid,best_ask,underlying_bid,underlying_ask,number_of_exchanges,exchange,bid_size,bid,ask_size,ask) FROM '/Users/snugs/Downloads/gmcr2/#{filename}' CSV")
    end
  end
end

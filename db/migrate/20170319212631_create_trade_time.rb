class CreateTradeTime < ActiveRecord::Migration
  def change
    # underlying_symbol,quote_datetime, sequence_number
    # root,expiration,strike,option_type,
    # exchange_id,trade_size,trade_price,trade_condition_id,canceled_trade_condition_id,best_bid,best_ask,
    # underlying_bid,underlying_ask,number_of_exchanges,exchange,bid_size,bid,ask_size,ask
    create_table :trade_times do |t|
      t.datetime  :at
      t.date :expiration
      t.decimal :strike
      t.string :option_type
      t.integer :trade_size
      t.decimal :trade_price
      t.decimal :bid
      t.decimal :ask
      t.integer :bid_size
      t.integer :ask_size
      t.decimal :best_bid
      t.decimal :best_ask
      t.decimal :underlying_bid
      t.decimal :underlying_ask
      t.integer :sequence
      t.integer :exchange_id
      t.integer :trade_condition_id
      t.integer :canceled_trade_condition_id
      t.integer :number_of_exchanges
      t.integer :exchange
      t.string :symbol
      t.string :root
    end
    #alter table trade_times alter column sequence type BIGINT;

    # remove_column :trade_times, :symbol
    # add_index :trade_times, :at
    # add_index :trade_times, :expiration
    # add_index :trade_times, :strike
  end
end

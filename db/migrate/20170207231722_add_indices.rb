class AddIndices < ActiveRecord::Migration
  def change
    add_index :spy_historic_options, :at
    add_index :spy_historic_options, :expiration
    add_index :spy_historic_options, :strike
  end
end

class CreateSpx < ActiveRecord::Migration
  def change
    create_table :spx, force: true do |t|
      t.datetime  :at
      t.date "expiration"
      t.decimal "strike"
      t.string "option_type"
      t.integer  "volume"
      t.decimal "bid"
      t.decimal "ask"
      t.integer "bid_size"
      t.integer "ask_size"
      t.decimal "underlying_price"
      t.decimal "underlying_bid"
      t.decimal "underlying_ask"
      t.decimal "iv"
      t.decimal "delta"
      t.decimal "gamma"
      t.decimal "theta"
      t.decimal "vega"
      t.decimal "rho"
      t.string :symbol
      t.string :root
      t.decimal :open
      t.decimal :high
      t.decimal :low
      t.decimal :close
      t.decimal :implied_underlying_price
    end

    # add_index :spx, :at
    # add_index :spx, :expiration
    # add_index :spx, :strike
  end
end
